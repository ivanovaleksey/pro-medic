#!/usr/bin/env ruby

require 'axlsx'
require 'logger'
require 'open-uri'
require 'nokogiri'
require 'ruby-progressbar'

class Script
  module VERSION
    MAJOR = 1
    MINOR = 0
    PATCH = 0

    STRING = [MAJOR, MINOR, PATCH].join(".")
  end

  def initialize
    @logger = Logger.new('script.log')
    @logger.level = Logger::INFO
  end

  def call
    surround_with_log do
      greetings
      surround_with_log('fetching products...') { fetch_products }
      surround_with_log('writing file...') { write_file }
    end
  end

  private

  def fetch_products
    @html = Nokogiri::HTML open(site)
    progressbar = ProgressBar.create title: 'Products', total: links.count, format: '%t: [%B] %p%% (%a)'
    @products = links.map do |link|
      progressbar.increment
      begin
        html = Nokogiri::HTML(open([[site, link].join(''), 'perpage=all'].join('&'))) { |config| config.huge }
        html.css('div.cat_list div.mcart_element').map { |product| product_hash product }
      rescue OpenURI::HTTPError => e
        @logger.error([site, link].join(''))
        @logger.error(e.message)
        @logger.error(e.backtrace.join("\n"))
        nil
      end
    end.flatten.compact.uniq
  end

  def write_file
    Axlsx::Package.new do |p|
      wb = p.workbook
      wb.add_worksheet do |sheet|
        sheet.add_row ['Наименование', 'Цена']
        @products.each do |product|
          sheet.add_row [product[:title], product[:price]] if product[:price]
        end
      end
      p.serialize filename
    end
  end

  def greetings
    text = <<-TEXT.gsub(/^\s*/, '')
      Welcome to pro-medic parser v%{version}
      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    TEXT
    puts format(text, version: VERSION::STRING)
  end

  def site
    'http://pro-medic.ru'
  end

  def filename
    format('pro-medic-%{date}.%{ext}', date: Date.today, ext: 'xlsx')
  end

  def format_price(price)
    price[/(.*) руб./]
    $1.strip.tr(' ', '').gsub("\u00A0", '')
  end

  def product_hash(product)
    title = product.at_css('div.name a').text
    price = begin
      format_price product.at_css('div.prices span.current-price').text
    rescue
      nil
    end
    { title: title, price: price }
  end

  def links
    @links ||= @html.css('ul.menu_list li div.submenu ul li a').map { |a| a['href'] }
  end

  def surround_with_log(message = '')
    @logger.info(['start', message].reject(&:empty?).join(' '))
    yield
    @logger.info(['done', message].reject(&:empty?).join(' '))
  end

end

unless defined?(Ocra)
  Script.new.call
  puts 'File has been successfully created. Press any key to exit.'
  gets
end
